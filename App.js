import React, { Component } from 'react';
import { AppRegistry, View, TouchableOpacity, Text, Dimensions, Image } from 'react-native';

import items from './items.json'

export default class FlexDirectionBasics extends Component {

  compareRandom(a, b) {
    return Math.random() - 0.5;
  }

  constructor(props) {
    super(props)

    this.state = {
      backgroundColor: 'yellow',
      itemWidth: Dimensions.get('window').width/4,
      items: [],
      borderW: 0.5,
      fSize: 14,
      level: 0,
    }

    this.lastIndex = null

    this.colors = [
      '#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
      '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
      '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
      '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
      '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
      '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
      '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
      '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
      '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
      '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'
      ];

    this.levelMap = [
      [0, 8],
      [8, 16]
    ]

  } 

  componentDidMount() {
    this.gameInit()
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  gameInit (level) {
    var itemsArr = items
      .slice(...this.levelMap[level || this.state.level])
      .reduce((list, item) => {
        return list.concat([
          {
            text:    item.en,
            color:   this.colors[this.getRandomInt(this.colors.length)],
            opacity: 0,
            opened:  0,
            url:     item.url
          }, {
            text:    item.en,
            color:   this.colors[this.getRandomInt(this.colors.length)],
            opacity: 0,
            opened:  0,
          }
        ])
      }, [])
      .sort(this.compareRandom);

    this.setState(prevState => {
      prevState.items = itemsArr;
      
      return prevState
    })
  }

  clickHandler(item, index) {
      
      let filteredItem = items.filter((it, i) => {
        return it.en == item.text;
      })

      if (filteredItem.length > 0) {
        let lastIndex = items.indexOf(filteredItem[0])

        if (lastIndex == this.lastIndex) {
          this.setState(prevState => {
            
            let items = prevState.items.filter(it => {
              return it.text == filteredItem[0].en
            })

            prevState.items[prevState.items.indexOf(items[0])].opened = 1            
            prevState.items[prevState.items.indexOf(items[1])].opened = 1            
            prevState.items[prevState.items.indexOf(items[0])].color = 'white'
            prevState.items[prevState.items.indexOf(items[1])].color = 'white'
            
            return prevState
          })
        } 

        this.setState(prevState => {
          let j = 0;

          for (let i=0; i < prevState.items.length; i++) {
            if (!prevState.items[i].opened) {
              prevState.items[i].opacity = 0
              j++
            }
          }

          if (j==0) {
            if (prevState.level+1 < this.levelMap.length) {
              prevState.level++;
              this.gameInit(prevState.level)
              alert('Congratulations! You moved to level ' + (prevState.level))
            } else {
              alert('cool man')
            }
          }

          return prevState
        })

        this.lastIndex = lastIndex
      }

      this.setState(prevState => {
        prevState.items[index].opacity = 1;

        return prevState;
      })
        
  }

  restartGame() {
    this.gameInit()
  }

  render() {
    
    return [
      <View style={{flex: 0, flexWrap: 'wrap', flexDirection: 'row', borderTopWidth: 50}}>
          {this.state.items.map((item, i) => {
            return <TouchableOpacity onPress={() => this.clickHandler(item, i) } key={i}>
              <View style={{width: this.state.itemWidth, 
                          height: this.state.itemWidth, 
                          borderWidth: this.state.borderW, 
                          }} key={i}>
                {item.url ? <Image 
                  style={{width: '100%', height: '100%', opacity: item.opacity, resizeMode: 'contain'}}
                  source={{uri: item.url}} key={i}/> : <Text style={{textAlign: 'center', 
                              opacity: item.opacity,
                              lineHeight: this.state.itemWidth,
                              fontSize: this.state.fSize}} key={i}>{item.text}</Text>}
              </View>
            </TouchableOpacity>
          })}
          
      </View>,
      <TouchableOpacity onPress={() => { this.restartGame() }} >
        <View marginTop={40} 
              alignContent='center'>
          <Text style={{textAlign: 'center', fontSize: 24}}>RESTART GAME</Text>
        </View>
      </TouchableOpacity>
    ]
  }
};


// skip this line if using Create React Native App
AppRegistry.registerComponent('AwesomeProject', () => FlexDirectionBasics);
